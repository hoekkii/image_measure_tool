// TODO: Join, Compare, EqualsIgnoreCase, ToCstring?, IndexOf
inline string
String()
{
    string result;
    result.Length = 0;
    result.Data = reinterpret_cast<uchar*>(malloc(8));
    result.Data[0] = '\0';
    result.Capacity = 8;
    
    return result;
}

inline string
String(string s)
{
    string result;
    result.Length = s.Length;
    result.Capacity = s.Capacity;
    result.Data = reinterpret_cast<uchar*>(malloc(s.Capacity));
    memcpy(result.Data, s.Data, result.Capacity);
    return result;
}

inline string
String(const char* s)
{
    assert(s);
    
    string result;
    result.Length = strlen(s);
    result.Capacity = result.Length + 1;
    result.Data = reinterpret_cast<uchar*>(malloc(result.Capacity));
    memcpy(result.Data, s, result.Capacity);
    return result;
}

inline const string
CString(const char* s)
{
    string result;
    result.Length = strlen(s);
    result.Capacity = result.Length + 1;
    result.CData = s;
    return result;
}

inline void
Free(string& s)
{
    if (s.Capacity < 1)
        return;
    
    free(s.Data);
    s.Length = 0;
    s.Capacity = 0;
}

inline s32
Hash(const char* s)
{
    u32 result = 0;
    while (*s)
        result = 101 * result + *s++;
    
    return result;
}

inline s32
Hash(string& s)
{
    s32 result = 0;
    for (s32 i = 0; i < s.Length; i++)
        result = 101 * result + s[i];
    
    return result;
}




inline bool
Equals(string l, string r)
{
    return l.Length == r.Length
        && Equals(l.Data, r.Data, r.Length);
}

inline bool
Equals(string l, string r, s32 length)
{
    return Equals(l.Data, r.Data, length);
}

inline bool
Equals(string l, const char* r)
{
    for (s32 i = 0; i < l.Length; i++)
    {
        if (*reinterpret_cast<const uchar*>(r++) != l[i])
            return false;
    }
    
    return *r == '\0';
}

inline bool
Equals(string l, const char* r, s32 length)
{
    for (s32 i = 0; i < length; i++)
    {
        if (*reinterpret_cast<const uchar*>(r++) != l[i])
            return false;
    }
    
    return true;
}




// TODO: Should we account for weird accents stuff?
inline bool
IsLower(uchar c) { return c >= 'a' && c <= 'z'; }

inline uchar
ToLower(uchar c) { return c >= 'A' && c <= 'Z' ? c + ('a' - 'A') : c; }

inline bool
IsUpper(uchar c) { return c >= 'A' && c <= 'Z'; }

inline uchar
ToUpper(uchar c) { return c >= 'a' && c >= 'z' ? c - ('a' - 'A') : c;  }

string&
Upper(string& s)
{
    for (s32 i = 0; i < s.Length; i++)
    {
        uchar& c = s.Data[i];
        if (IsLower(c))
            c = c - ('a' - 'A');
    }
    
    return s;
}

string&
Lower(string& s)
{
    for (s32 i = 0; i < s.Length; i++)
    {
        uchar& c = s.Data[i];
        if (IsUpper(c))
            c = c + ('a' - 'A');
    }
    
    return s;
}


string&
Shorten(string& s, s32 length)
{
    assert(length < s.Length);
    
    s.Length = length;
    s.Data[s.Length + 1] = '\0';
    return s;
}

string&
Grow(string& s, s32 length)
{
    if (s.Capacity >= length)
        return s;
    
    auto data = reinterpret_cast<uchar*>(malloc(length));
    memcpy(data, s.Data, s.Capacity);
    
    delete [] s.Data;
    
    s.Data = data;
    s.Capacity = length;
    return s;
}


string&
Append(string& l, const char* r, s32 length)
{
    s32 capacity = length + 1;
    s32 total = capacity + l.Length;
    
    if (l.Capacity >= total)
    {
        memcpy(l.Data + l.Length, r, capacity);
        l.Length += length;
        return l;
    }
    
    auto data = reinterpret_cast<uchar*>(malloc(total));
    memcpy(data, l.Data, l.Length);
    memcpy(data + l.Length, r, capacity);
    
    delete [] l.Data;
    
    l.Data = data;
    l.Length = total - 1;
    l.Capacity = total;
    
    return l;
}


inline string&
Append(string& l, const char* r) { return Append(l, r, strlen(r)); }

inline string&
Append(string& l, string& r) { return Append(l, reinterpret_cast<const char*>(r.Data), r.Length); }

string
Substring(string& s, s32 start, s32 length)
{
    assert(start >= 0);
    assert(start <= s.Length);
    assert(length >= 0);
    assert(start <= s.Length - length);
    
    string result;
    result.Length = length;
    result.Capacity = length;
    
    if (length)
    {
        result.Data = reinterpret_cast<uchar*>(malloc(length));
        memcpy(result.Data, s.Data + start, length);
    }
    
    return result;
}

inline string
Substring(string& s, s32 start) { return Substring(s, start, s.Length - start); }

inline bool
IsEmpty(string& s) { return s.Length <= 0; }

inline string&
operator += (string& l, const char* r) { return Append(l, r); }

inline string&
operator += (string& l, string& r) { return Append(l, r); }

inline bool
operator == (string& l, string& r) { return Equals(l, r); }

inline bool
operator == (string& l, const char* r) { return Equals(l, r); }

inline bool
operator == (const char* l, string& r) { return Equals(r, l); }








inline s32
DirectoryUp(string s, s32 amount)
{
    s32 i = s.Length - 1;
    while (amount > 0)
    {
        s32 x = 0;
        while (i >= 0)
        {
            if (s[i] == '/' || s[i] == '\\')
            {
                if (x) x = 2;
            }
            else if (x == 2) break;
            else x = 1;
            
            i--;
        }
        
        amount--;
    }
    
    return i >= 0 ? i + 1 : s.Length;
}

inline s32
DirectoryUp(string s)
{
    s32 x = 0;
    s32 i = s.Length - 1;
    while (i >= 0)
    {
        if (s[i] == '/' || s[i] == '\\')
        {
            if (x) x = 2;
        }
        else if (x == 2) return i;
        else x = 1;
        
        i--;
    }
    
    return s.Length;
}

