// https://sites.google.com/site/murmurhash/
s32 MurmurHash2(const uchar* data, s32 len, u32 seed = 0)
{
    const s32 m = 0x5BD1E995;
    const s32 r = 24;
    u32 h = seed ^ len;
    while (len >= 4)
    {
        s32 k = *reinterpret_cast<const s32*>(data);
        k *= m;
        k ^= k >> r;
        k *= m;
        h *= m;
        h ^= k;
        
        data += 4;
        len -= 4;
    }
    
    switch (len)
    {
        case 3: h ^= data[2] << 16;
        case 2: h ^= data[1] << 8;
        case 1: h ^= data[0];
        h *= m;
    }
    
    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;
    
    return h;
}

inline s32
HashOf(const void* key, s32 len)
{
    return MurmurHash2(reinterpret_cast<const uchar*>(key), len, 0);
}

inline bool
Equals(const void* l, const void* r, s32 length)
{
    const uchar* L = reinterpret_cast<const uchar*>(l);
    const uchar* R = reinterpret_cast<const uchar*>(r);
    while (length >= 4)
    {
        if (*reinterpret_cast<const u32*>(L) != *reinterpret_cast<const u32*>(R))
            return false;
        
        L += 4;
        R += 4;
        length -= 4;
    }
    
    switch (length)
    {
        case 3: if (L[2] != R[2]) return false;
        case 2: if (L[1] != R[1]) return false;
        case 1: if (L[0] != R[0]) return false;
    }
    
    return true;
}


inline void Reserve(buffer& b, s32 size)
{
    if (b.Length < size)
    {
        auto data = reinterpret_cast<u8*>(malloc(size));
        memcpy(data, b.Data, b.Length);
        delete [] b.Data;
        b.Data = data;
    }
}

inline void Reserve(buffer& b)
{
    Reserve(b, b.Length * 2);
}


