#include <SDL2/SDL.h>
#include <cmath>
#include <png.h>

#define IMT_INTERNAL 1
#define IMT_DEBUG 1
#define IMT_TMP_BASE_CAPACITY 4096
#define IMT_TMP_STACK_CAPACITY 32
#define IMT_PLATFORM_DATA "linux/imt.h"

#include "types.h"
#include "linux/types.h"
#include "imt_t.h"
#include "math.cpp"
#include "utils.cpp"
#include "string.cpp"
#include "imt.cpp"
#include "linux/render.cpp"




inline int __ExitWithSdlError(const char* file, u32 line, const char* message) { fprintf(stderr, "[%s : %u] %s Error: %s\n", file, line, message, SDL_GetError()); return 1; }
#define ExitWithSdlError(msg) __ExitWithSdlError(__FILE__, __LINE__, #msg )

inline void
Sleep(u32 ms) { SDL_Delay(ms); }

int main(s32 argc, const char* args[])
{
    // Default tests
    assert(argc > 0);
    assert(sizeof(char) == 1);
    assert(sizeof(Rect) == sizeof(SDL_Rect));
    
    if (SDL_Init(SDL_INIT_EVERYTHING))
        return ExitWithSdlError("SDL_Init");
    
    IMT imt { 0 };
    Initialize(imt);
    imt.App.Version = CString("0.1");
    imt.Window = SDL_CreateWindow(
        "Solitude",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        720,
        720,
        SDL_WINDOW_RESIZABLE);
    
    if (!imt.Window)
        return ExitWithSdlError("SDL_CreateWindow");
    
    imt.Renderer = SDL_CreateRenderer(imt.Window, -1, SDL_RENDERER_ACCELERATED);
    if (!imt.Renderer)
        return ExitWithSdlError("SDL_CreateRenderer");
    SDL_SetRenderDrawColor(imt.Renderer, 0x1e, 0x1e, 0x1e, 0xFF);
    
    
    // Path
    auto tmp = PushTmp<char>(imt, PATH_MAX);
    realpath(args[0], tmp);
    printf("%s\n", tmp);
    imt.App.Path = String(tmp);
    // TODO: Assetpath
    Append(imt.App.Path, "/");
    PopTmp(imt);
    
    auto test = 'a';
	
    // Main game loop
    for (bool running = true; running;)
    {
        s32 i;
        SDL_Event event;
        for (i = 0; i < 1000 && SDL_PollEvent(&event); ++i)
        {
            switch (event.type)
            {
                case SDL_KEYDOWN: {
                    running = false;
                } break;
                
                // https://wiki.libsdl.org/SDL_MouseMotionEvent
                case SDL_MOUSEMOTION: {
                    auto motion = event.motion;
                    //print("motion: %i, %i\n",motion.x, motion.y);
                } break;
                
                // https://wiki.libsdl.org/SDL_MouseButtonEvent
                case SDL_MOUSEBUTTONDOWN: {
                    auto button = event.button;
                    print("down: %i, %i, %i\n", button.x, button.y, button.button);
                } break;
                
                case SDL_MOUSEBUTTONUP: {
                    auto button = event.button;
                    print("up: %i, %i, %i\n", button.x, button.y, button.button);
                } break;
                
                case SDL_MOUSEWHEEL: {
                    print("wheel: \n");
                } break;
                
                /*case SDL_MOUSEWHEEL: {
                    auto wheel = event.wheel;
                    print("%f, %f\n",wheel.motion.x, wheel.motion.y);
                } break;*/
            }
        }
        
        if (i) Render(imt);
        Sleep(8);
    }
    
    return 0;
}