inline v2
operator * (s32 l, v2 r)
{
    return {
        l * r.x,
        l * r.y
    };
}
inline v2
operator * (v2 l, s32 r)
{
    return {
        l.x * r,
        l.y * r
    };
}
inline v2&
operator *= (v2& l, s32 r)
{
    l.x *= r;
    l.y *= r;
    return l;
}
inline v2
operator / (v2 l, s32 r)
{
    return {
        l.x / r,
        l.y / r
    };
}
inline v2&
operator /= (v2& l, s32 r)
{
    l.x /= r;
    l.y /= r;
    return l;
}
inline v2
operator - (v2 l)
{
    return {
        -l.x,
        -l.y
    };
}
inline v2
operator - (v2 l, v2 r)
{
    return {
        l.x - r.x,
        l.y - r.y
    };
}
inline v2&
operator -= (v2& l, v2 r)
{
    l.x -= r.x;
    l.y -= r.y;
    return l;
}
inline v2
operator + (v2 l, v2 r)
{
    return {
        l.x + r.x,
        l.y + r.y
    };
}
inline v2&
operator += (v2& l, v2 r)
{
    l.x += r.x;
    l.y += r.y;
    return l;
}

/*
inline s8
Clamp(s8 x, s8 min, s8 max) { return x < min ? min : x > max ? max : x; }
inline s16
Clamp(s16 x, s16 min, s16 max) { return x < min ? min : x > max ? max : x; }
inline s32
Clamp(s32 x, s32 min, s32 max) { return x < min ? min : x > max ? max : x; }
inline s64
Clamp(s64 x, s64 min, s64 max) { return x < min ? min : x > max ? max : x; }
inline s8
Clamp(u8 x, u8 min, u8 max) { return x < min ? min : x > max ? max : x; }
inline s16
Clamp(u16 x, u16 min, u16 max) { return x < min ? min : x > max ? max : x; }
inline u32
Clamp(u32 x, u32 min, u32 max) { return x < min ? min : x > max ? max : x; }
inline u64
Clamp(u64 x, u64 min, u64 max) { return x < min ? min : x > max ? max : x; }
inline r32
Clamp(r32 x, r32 min, r32 max) { return x < min ? min : x > max ? max : x; }
inline r64
Clamp(r64 x, r64 min, r64 max) { return x < min ? min : x > max ? max : x; }
inline v2
Clamp(v2 v, v2 min, v2 max)
{
    return {
        Clamp(v.x, min.x, max.x),
        Clamp(v.y, min.y, max.y)
    };
}

inline s32
Clamp01(s32 x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline r32
Clamp01(r32 x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline r64
Clamp01(r64 x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline v2
Clamp01(v2 v) { return { Clamp01(v.x), Clamp01(v.y) }; }


inline r32
Degrees(r32 x) { return x * RAD2DEG; }


inline s32
Dot(v2 x0, v2 x1) { return x0.x * x1.x + x0.y * x1.y; }



inline v2
Hadamard(v2 l, v2 r)
{
    return {
        l.x * r.x,
        l.y * r.y
    };
}


inline r32
Length(v2 v) { return Sqrt(v.x * v.x + v.y * v.y); }


inline r32
Lerp(r32 l, r32 r, r32 t) { return l + Clamp01(t) * (r - l); }

inline r32
Lerpu(r32 l, r32 r, r32 t) { return l + t * (r - l); }


inline r32
Magnitude(v2 v) { return Sqrt(v.x * v.x + v.y * v.y); }


inline r32
Max(r32 l, r32 r) { return l > r ? l : r; }
inline v2
Max(v2 l, v2 r)
{
    return {
        Max(l.x, r.x),
        Max(l.y, r.y)
    };
}



inline r32
Min(r32 l, r32 r) { return l < r ? l : r; }
inline v2
Min(v2 l, v2 r)
{
    return {
        Min(l.x, r.x),
        Min(l.y, r.y)
    };
}


inline r32
Pow(r32 l, r32 r) { return powf(l, r); }




inline r32
Radians(r32 x) { return x * DEG2RAD; }



inline r32
Sign(r32 l) { return l < 0.0f ? -1.0f : l > 0.0f ? 1.0f : 0.0f; }
inline s32
Sign(s32 l) { return l < 0 ? -1 : l > 0 ? 1 : 0; }
inline v2
Sign(v2 l) {
    return {
        Sign(l.x),
        Sign(l.y)
    };
}


inline r32
Smoothstep(r32 x, r32 min, r32 max) { 
    r32 t = Clamp01((x - min) / (max - min));
    return t * t * (3.0f - (2.0f * t));
}



inline r32
SqrDistance(v2 v0, v2 v1)
{
    v2 diff { v0.x - v1.x, v0.y - v1.y };
    return diff.x * diff.x + diff.y * diff.y;
}


inline r32
SqrMagnitude(r32 v) { return v * v; }
inline r32
SqrMagnitude(v2 v) { return v.x * v.x + v.y * v.y; }


inline v2
Unit2(s32 x) { return { x, x }; }
*/



