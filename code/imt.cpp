inline void Initialize(IMT& imt)
{
    imt.Tmp.Data = new u8[IMT_TMP_BASE_CAPACITY];
    imt.Tmp.Length = IMT_TMP_BASE_CAPACITY;
}

template<typename T>
inline T* PushTmp(IMT& imt, s32 length)
{
    assert(imt.TmpIndex < IMT_TMP_STACK_CAPACITY - 1);
    assert(length >= 0);
    
    auto size = sizeof(T) * length;
    auto used = imt.TmpStack[imt.TmpIndex];
    Reserve(imt.Tmp, used + size);
    
    auto result = &imt.Tmp.Data[used];
    imt.TmpIndex++;
    imt.TmpStack[imt.TmpIndex] = used + length;
    
    return reinterpret_cast<T*>(result);
}

inline void PopTmp(IMT& imt)
{
    imt.TmpStack[imt.TmpIndex--] = 0;
}

inline void ClearTmp(IMT& imt)
{
    for (auto i = imt.TmpIndex; i >= 0; i--)
        imt.TmpStack[i] = 0;
    
    imt.TmpIndex = 0;
}