
#include <windows.h>
#include <stdio.h>
#include <cmath>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <intrin.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <limits.h>
#include <float.h>
#include <stdarg.h>

#define IMT_INTERNAL 1
#define IMT_DEBUG 1
#define IMT_TMP_BASE_CAPACITY 4096

#include "types.cpp"


LRESULT CALLBACK MainWindowCallback(
HWND window,
UINT message,
WPARAM wparam,
LPARAM lparam)
{
	LRESULT result = 0;
	switch(message)
	{
		case WM_SIZE: {
			OutputDebugStringA("WM_SIZE\n");
		} break;
		
		case WM_CLOSE: {
			OutputDebugStringA("WM_CLOSE\n");
		} break;
		
		case WM_ACTIVATEAPP: {
			OutputDebugStringA("WM_ACTIVATEAPP\n");
		} break;
		
		case WM_DESTROY: {
			OutputDebugStringA("WM_DESTROY\n");
		} break;
		
		/*case WM_PAINT: {
   PAINTSTRUCT paint;
   HDC context = BeginPaint(window, &paint);
   auto x = paint.rcPaint.left;
   auto y = paint.rcPaint.top;
   auto width = paint.rcPaint.right - x;
   auto height = paint.rcPaint.bottom - y;
   //PatBlt(context, x, y, width, height, WHITENESS);
   EndPaint(window, &paint);
   
  } break;*/
		
		default: {
			result = DefWindowProc(window, message, wparam, lparam);
		} break;
	}
	
	
	return result;
}

extern "C" int __stdcall WinMainCRTStartup()
{
	HINSTANCE instance = GetModuleHandle(0);
    
	WNDCLASSA windowClass { };
	windowClass.style = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
	windowClass.lpfnWndProc = MainWindowCallback;
	windowClass.lpszClassName = "IMT_WindowClass";
	if(RegisterClassA(&windowClass))
	{
		HWND window = CreateWindowExA(
			0, // WS_EX_TOPMOST|WS_EX_LAYERED,
			windowClass.lpszClassName,
			"IMT",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			0,
			0,
			instance,
			0);
		
		if (window)
		{
			for (;;)
			{
				MSG message;
				BOOL result = GetMessageA(&message, 0, 0, 0);
				if (result > 0)
				{
					TranslateMessage(&message);
					DispatchMessageA(&message);
				}
				else
				{
					break;
				}
			}
			
			
		}
	}
	
	return 0;
}
