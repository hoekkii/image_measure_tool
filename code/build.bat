@echo off

REM Init command line
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64


REM Checking directory
IF NOT EXIST ..\build mkdir ..\build
pushd ..\build
del *.pdb > NUL 2> NUL
del *.exe > NUL 2> NUL

REM Flags
REM set CommonCompilerFlags=-WL -Od -nologo -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4505 -wd4127 -FC -Z7 -GS- -Gs9999999
REM set CommonCompilerFlags=-HOEKKII_DEBUG=1 %CommonCompilerFlags%
set CommonLinkerFlags= -STACK:0x100000,0x100000 -incremental:no -opt:ref user32.lib winmm.lib kernel32.lib


REM cl %CommonCompilerFlags% ../code/imt_windows.cpp /link /NODEFAULTLIB /SUBSYSTEM:windows %CommonLinkerFlags%
cl ../code/imt_windows.cpp /link /SUBSYSTEM:windows kernel32.lib libucrt.lib user32.lib winmm.lib

IF EXIST imt_windows.exe call imt_windows.exe
popd