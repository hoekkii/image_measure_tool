#!/bin/bash

run=0

for arg_i in "$@"
do
	if [ "$arg_i" = "-r" ]
		then
			run=1
	fi
done


cdir=$(dirname "${BASH_SOURCE[0]}")
file="/imt_linux.cpp"
rdir="/../build/imt"

echo "compiling \"$cdir$file\" to \"$cdir$rdir\""
g++ "$cdir$file" $(pkg-config --cflags --libs sdl2 libpng) -m64 -o "$cdir$rdir"

if [ $run = 1 ]
	then
		$cdir$rdir
fi
