// Base types
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int32_t b32;
typedef float r32;
typedef double r64;
typedef intptr_t intptr;
typedef uintptr_t uintptr;
typedef unsigned char uchar;

// Defines
#define null nullptr
#define var auto
#define let auto

// Constants
#define PI 3.141592653589793f
#define TAU 6.283185307179586f
#define DEG2RAD 0.017453292519943295f
#define RAD2DEG 57.29577951308232f
#define EPSILON 0.00001f
#define S8MIN  -128
#define S16MIN -32768
#define S32MIN -2147483648
#define S64MIN -9223372036854775808L
#define S8MAX  127
#define S16MAX 32767
#define S32MAX 2147483647
#define S64MAX 9223372036854775807L
#define U8MAX  255
#define U16MAX 65535
#define U32MAX 4294967295U
#define U64MAX 18446744073709551615UL

// assert and s(tatic)_assert
#define CRASH *(int*)0=0
#if IMT_DEBUG
#define assert(expr) if (!(expr)) { fprintf(stderr, "FAILED assertion [" __FILE__ ":%i] " #expr "\n", __LINE__);  CRASH; }
#define s_assert(expr) static_assert(expr, "FAILED assertion: " #expr)
#else
#define assert(expr)
#define s_assert(expr)
#endif

#if IMT_INTERNAL
#define NotImplemented assert(!"NotImplemented")
#else
#define NotImplemented NotImplemented!
#endif

#define print(...) fprintf(stderr, __VA_ARGS__)
#define PPCAT_NX(A, B) A ## B
#define PPCAT(A, B) PPCAT_NX(A, B)

#define ARGUMENT_AT_0(arg0, ...) arg0
#define ARGUMENT_AT_1(arg0, arg1, ...) arg1
#define ARGUMENT_AT_2(arg0, arg1, arg2, ...) arg2
#define ARGUMENT_AT_3(arg0, arg1, arg2, arg3, ...) arg3
#define ARGUMENT_AT_4(arg0, arg1, arg2, arg3, arg4, ...) arg4

#define For_0(arr, i) for (s32 i = 0; i < arr.Length; ++i)
#define For_1(arr) for (s32 i = 0; i < arr.Length; ++i)
#define For(...) ARGUMENT_AT_2(__VA_ARGS__, For_0, For_1)(__VA_ARGS__)

// Forr(eversed)
#define Forr_0(arr, i) for (s32 i = arr.Length - 1; i >= 0; --i)
#define Forr_1(arr) for (s32 i = arr.Length - 1; i >= 0; --i)
#define Forr(...) ARGUMENT_AT_2(__VA_ARGS__, Forr_0, Forr_1)(__VA_ARGS__)


struct buffer
{
    u8* Data;
    s32 Length;
    
    inline u8& operator [] (s32 i)
    {
        assert(i >= 0);
        assert(i < this->Length);
        return this->Data[i];
    }
};

struct varbuffer : public buffer
{
    s32 Capacity;
};

struct string
{
    union
    {
        uchar* Data;
        u8* UData;
        char* SData;
        const char* CData;
        void* VData;
    };
    
    s32 Length;
    s32 Capacity;
    
    inline uchar& operator [] (s32 i)
    {
        assert(i >= 0);
        assert(i < this->Capacity);
        return this->Data[i];
    }
};

union v2
{
    struct
    {
        s32 x;
        s32 y;
    };
    
    struct
    {
        s32 u;
        s32 v;
    };
	
    struct
    {
        s32 width;
        s32 height;
    };
    
    s32 e[2];
};

union Rect
{
	struct
	{
		s32 x;
		s32 y;
		
		union
		{
			s32 w;
			s32 width;
		};
        
		union
		{
			s32 h;
			s32 height;
		};
	};
    
	struct
	{
		v2 position;
		v2 size;
	};
    
    // This is for easy sdl use
    SDL_Rect sdl;
    
    s32 e[4];
};
